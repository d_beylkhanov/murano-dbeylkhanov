﻿using System.ComponentModel.DataAnnotations;

namespace MuranoWeb.Data.Entity
{
    public class Quote
    {
        public int Id { get; set; }

        [Required]
        public string Author { get; set; }
        [Required]
        public string Text { get; set; }
    }
}