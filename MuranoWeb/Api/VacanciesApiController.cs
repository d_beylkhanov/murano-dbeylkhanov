﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MuranoWeb.Api.Models;
using MuranoWeb.Data.Entity;
using MuranoWeb.Data.Extensions;
using MuranoWeb.Data.Repositories;
using MuranoWeb.Utils;
using NLog;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MuranoWeb.Api
{
    [Authorize]
    [Route("admin/api/vacancies")]
    public class VacanciesApiController : Controller
    {
        private readonly IRepository<Vacancy> _repository;
        private readonly IRepository<VacancyLog> _logRepository;
        private readonly IRepository<VacancyIcon> _iconRepository;
        private Logger _logger = LogManager.GetCurrentClassLogger();

        public VacanciesApiController(IRepository<Vacancy> repository, IRepository<VacancyLog> logRepository, IRepository<VacancyIcon> iconRepository)
        {
            _repository = repository;
            _logRepository = logRepository;
            _iconRepository = iconRepository;
        }

        [HttpGet]
        public Task<List<VacancyModel>> GetAll(Location? location)
        {
            IQueryable<Vacancy> query = _repository.GetAll().OrderBy(x => x.Order);
            if (location.HasValue)
            {
                query = query.Where(x => x.Location == location);
            }
            return query.ToAsyncEnumerable().Select(x => new VacancyModel
            {
                Id = x.VacancyId,
                Title = x.Title,
                Location = x.Location.DisplayName(),
                InArchive = x.InArchive
            }).ToList();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            _logger.Debug("GetVacancyById: "+id);
            try
            {
                Vacancy vacancy = await _repository.GetById(id).ConfigureAwait(false);
                

                if (vacancy == null)
                {
                    _logger.Debug("Vacancy " + id + " not found");
                    return NotFound();
                }
                _repository.GetContext().Entry(vacancy).Reference("Icon").Load();

                return new ObjectResult(new VacancyFullModel
                {
                    Id = vacancy.VacancyId,
                    LocationEnum = vacancy.Location,
                    Location = vacancy.Location.DisplayName(),
                    Description = vacancy.Description,
                    Requirements = vacancy.Requirements,
                    Summary = vacancy.Summary,
                    Title = vacancy.Title,
                    InArchive = vacancy.InArchive,
                    IconId = vacancy.Icon.Id
                });
            }
            catch (Exception ex)
            {
                _logger.Debug(ex, "GetVacancyById:");
                return Ok(ex.ToString());
            }
          
        }

        [HttpPost]
        public async Task<IActionResult> AddNew([FromBody] VacancyFullModel model)
        {
            if (model == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            VacancyIcon defaultIcon = await _iconRepository.GetById(_iconRepository.GetAll().Min(x => x.Id));
            var icon = await _iconRepository.GetById(model.IconId);

            Vacancy created = await _repository.Add(new Vacancy
            {
                Location = model.LocationEnum,
                Description = model.Description,
                Requirements = model.Requirements,
                Summary = model.Summary,
                Title = model.Title,
                Icon = icon ?? defaultIcon
            });

            await logVacancy(created.VacancyId, VacancyAction.Create);

            return CreatedAtAction(nameof(VacanciesApiController.GetById), new { Id = created.VacancyId });
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] VacancyFullModel model)
        {
            if (model == null || model.Id != id || !ModelState.IsValid)
            {
                return BadRequest();
            }

            Vacancy vacancy = await _repository.GetById(id);
            vacancy.Location = model.LocationEnum;
            vacancy.Description = model.Description;
            vacancy.Requirements = model.Requirements;
            vacancy.Summary = model.Summary;
            vacancy.Title = model.Title;
            vacancy.Icon = await _iconRepository.GetById(model.IconId);

            await logVacancy(vacancy.VacancyId, VacancyAction.Update);

            await _repository.Update(vacancy);

            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> InArchive(int id)
        {
            Vacancy vacancy = await _repository.GetById(id);
            if(vacancy == null)
            {
                return NotFound();
            }

            vacancy.InArchive = true;

            await _repository.Update(vacancy);

            await logVacancy(vacancy.VacancyId, VacancyAction.Archive);

            return new NoContentResult();
        }

        [HttpOptions("{id}")]
        public async Task<IActionResult> Open(int id)
        {
            Vacancy vacancy = await _repository.GetById(id);
            if (vacancy == null)
            {
                return NotFound();
            }

            vacancy.InArchive = false;

            await _repository.Update(vacancy);

            await logVacancy(vacancy.VacancyId, VacancyAction.Open);

            return new NoContentResult();
        }

        [HttpGet("{id}/log")]
        public Task<List<VacancyLogModel>> GetLogs(int id)
        {
            return _logRepository.GetAll().Where(x => x.VacancyId == id).ToAsyncEnumerable().Select(x => new VacancyLogModel(x)).OrderByDescending(x => x.Date).Take(10).ToList();
        }

        [HttpPatch("sort/{location}")]
        public async Task<IActionResult> UpdateOrder(Location location, [FromBody]int[] ids)
        {
            if (ids != null && ids.Any())
            {
                for (int i = 0; i < ids.Length; i++)
                {
                    var vacancy = await _repository.GetById(ids[i]);
                    if (vacancy != null && vacancy.Order != i)
                    {
                        vacancy.Order = i;
                        await _repository.Update(vacancy);
                    }
                }
            }
            return Ok();
        }

        private async Task logVacancy(int vacancyId, VacancyAction action)
        {
            await _logRepository.Add(new VacancyLog { VacancyId = vacancyId, User = User.Identity.Name, Action = action, Date = DateTime.Now }).ConfigureAwait(false);
        }
    }
}
