﻿import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MainNavigationComponent } from './main-navigation/main-navigation.component';
import { VacanciesListComponent } from './vacancies-list/vacancies-list.component';
import { JobApplicationsListComponent } from './job-applications-list/job-applications-list.component';
import { VacancyEditComponent } from './vacancy-edit/vacancy-edit.component';
import { LocationSelectorComponent } from './location-selector/location-selector.component';
import { PhotosListComponent } from './photos-list/photos-list.component';
import { IconsListComponent } from './icons-list/icons-list.component';

import { AppRoutes } from './routes'; 

import { VacanciesService } from './services/vacancies.service';
import { OfficeLocationsService } from './services/office.locations.service';
import { PhotoService } from './services/photo.service';
import { IconService } from './services/icons.service';
import { FileChooserComponent } from './file-chooser/file-chooser.component';
import { QuoteService } from './services/quote.service';
import { QuotesListComponent } from './quotes-list/quotes-list.component';
import { ModalComponent } from './modal/modal.component';
import { JobApplicationService } from './services/job.application.service';
import { TinymceModule } from 'angular2-tinymce';

@NgModule({
  declarations: [
    AppComponent,
    MainNavigationComponent,
    VacanciesListComponent,
    JobApplicationsListComponent,
    VacancyEditComponent,
    LocationSelectorComponent,
    IconsListComponent,
    FileChooserComponent,
    PhotosListComponent,
    QuotesListComponent,
    ModalComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
      FormsModule,
      TinymceModule.withConfig({
          menubar: "file edit format",
          menu: {
              file: { title: 'File', items: 'newdocument' },
              edit: { title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall' },
              format: { title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat' },
          },
          block_formats: 'Header=h4',
          style_formats: [
              { title: 'Header', block: 'h4'},
              { title: 'Bold text', inline: 'strong' }
          ],
          forced_root_block: 'p'
      }),
    RouterModule.forRoot(AppRoutes, { useHash: true })
  ],
  providers: [VacanciesService, OfficeLocationsService, PhotoService, IconService, QuoteService, JobApplicationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
