import { DataService } from './data.service';
import { Quote } from './quote';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class QuoteService extends DataService<Quote> {
    constructor(http: Http) {
        super(http, 'admin/api/quotes');
    }

    public delete(id: number): Promise<any> {
        return this.http.delete(`${this.url}/${id}`).toPromise().catch(super.handleError);
    }
}
