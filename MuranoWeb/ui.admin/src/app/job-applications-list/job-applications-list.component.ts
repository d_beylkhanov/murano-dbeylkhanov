import { Component, OnInit } from '@angular/core';
import { JobApplicationService } from '../services/job.application.service';
import { JobApplication } from '../services/job.application';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-job-applications-list',
  templateUrl: './job-applications-list.component.html',
  styleUrls: ['./job-applications-list.component.css']
})
export class JobApplicationsListComponent implements OnInit {
  applications: JobApplication[];
  constructor(private service: JobApplicationService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      const conditions: { [s: string]: string; } = {};
      const vacancyId = +params['vacancy'];
      if (vacancyId) {
        conditions.vacancyId = vacancyId.toString();
      }

      this.service.getAll(conditions).then(x => this.applications = x);
    });
  }
}
