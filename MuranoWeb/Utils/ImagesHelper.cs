﻿using MuranoWeb.Data.Entity;
using System.IO;
using System.Drawing;
using System.Linq;

namespace MuranoWeb.Utils
{
    public class ImagesHelper
    {
        public const int ImagesLimit = 15;

        private const string Path = "images/photo";
        private const string PreviewPath = "preview";
        private const string IconsPath = "images/icons";
        private const int PreviewWidth = 500;

        public static string GetImagesDirectory(string contentRootPath, Location location)
        {
            string directory = $"{contentRootPath}/{Path}/{location.ToString()}";
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            return directory;
        }

        public static string GetIconsDirectory(string contentRootPath)
        {
            string directory = $"{contentRootPath}/{IconsPath}";
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            return directory;
        }


        public static string GetImagesPreviewDirectory(string contentRootPath, Location location)
        {
            string directory = $"{contentRootPath}/{Path}/{location.ToString()}/{PreviewPath}";
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            return directory;
        }

        public static string GetImageUrl(string fileName, Location location)
        {
            return $"/{Path}/{location.ToString()}/{fileName}";
        }

        public static string GetIconUrl(string fileName)
        {
            return $"/{IconsPath}/{fileName}";
        }

        public static string GetIconUrl(VacancyIcon icon)
        {
            return $"/{IconsPath}/{icon.Name}";
        }

        public static string GetImagePreviewUrl(string fileName, Location location)
        {
            return $"/{Path}/{location.ToString()}/{PreviewPath}/{fileName}";
        }

        public static string RenameIfExist(string directory, string fileName)
        {
            var name = System.IO.Path.GetFileNameWithoutExtension(fileName);
            var ext = System.IO.Path.GetExtension(fileName);
            var index = 1;
            var path = System.IO.Path.Combine(directory, fileName);
            while (File.Exists(path))
            {
                fileName = $"{name}({index}){ext}";
                path = System.IO.Path.Combine(directory, fileName);
                index++;
            }
            return fileName;
        }

        public static string GeneratePreview (string contentRootPath, string imageFilename, Location location)
        {
            var srcDirectory = GetImagesDirectory(contentRootPath, location);
            var previewDirectory = GetImagesPreviewDirectory(contentRootPath, location);
            var previewImageFileName = RenameIfExist(previewDirectory, imageFilename);

            using (var image = new Bitmap(System.Drawing.Image.FromFile(System.IO.Path.Combine(srcDirectory, imageFilename))))
            {
                if (image.Width <= PreviewWidth)
                {
                    System.IO.File.Copy(System.IO.Path.Combine(srcDirectory, imageFilename), System.IO.Path.Combine(previewDirectory, previewImageFileName));
                }
                else
                {
                    var height = (int)(image.Height * PreviewWidth / (double)image.Width);
                    using (var resized = new Bitmap(PreviewWidth, height))
                    {
                        using (var graphics = Graphics.FromImage(resized))
                        {
                            graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighSpeed;
                            graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                            graphics.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
                            graphics.DrawImage(image, 0, 0, PreviewWidth, height);
                            using (var output = File.Open(System.IO.Path.Combine(previewDirectory, previewImageFileName), FileMode.Create))
                            {
                                var qualityParamId = System.Drawing.Imaging.Encoder.Quality;
                                var encoderParameters = new System.Drawing.Imaging.EncoderParameters(1);
                                encoderParameters.Param[0] = new System.Drawing.Imaging.EncoderParameter(qualityParamId, 8);
                                var codec = System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders().FirstOrDefault(c => c.FormatID == System.Drawing.Imaging.ImageFormat.Jpeg.Guid);
                                resized.Save(output, codec, encoderParameters);
                            }
                        }
                    }
                }
            }

            return previewImageFileName;
        }
    }
}
