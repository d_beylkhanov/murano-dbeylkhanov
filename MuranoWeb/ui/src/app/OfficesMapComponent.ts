import {
    eventOn,
    getElementsList
} from './DomUtils';

/**
 * @class ChartComponent
 * @public
 */
export default class OfficesMapComponent {

    private linksMap: Map<string, HTMLElement> = new Map<string, HTMLElement>();
    private pointsMap: Map<string, HTMLElement> = new Map<string, HTMLElement>();

    /**
     * @constructor
     * @param {Element} officesMapElement
     * @public
     */
    public constructor(officesMapElement: Element) {
        let links: NodeListOf<Element> = getElementsList('[data-js="officesLinks"] [data-point]', officesMapElement);
        let points: NodeListOf<Element> = getElementsList('[data-js="officesPoints"] [data-point]', officesMapElement);

        for (let i: number = 0; i < links.length; i++) {
            this.linksMap.set(links[i].getAttribute('data-point'), <HTMLElement>links[i]);
            this.bindLinkEventHandlers(<HTMLElement>links[i]);
        }

        for (let i: number = 0; i < points.length; i++) {
            this.pointsMap.set(points[i].getAttribute('data-point'), <HTMLElement>points[i]);
            this.bindPointEventHandlers(<HTMLElement>points[i]);
        }
    }

    /**
     * @method setOpacity
     * @param {HTMLElement} point
     * @param {string} value
     * @private
     */
    private setOpacity(point: HTMLElement, value: string): void {
        if (point) {
            point.style.opacity = value;
        }
    }

    /**
     * @method bindLinkEventHandlers
     * @param {Element} linkElement
     * @private
     */
    private bindLinkEventHandlers(linkElement: Element): void {

        eventOn(linkElement, 'mouseenter', (e: Event) => {
            e.preventDefault();
            let city: string = linkElement.getAttribute('data-point');
            this.setOpacity(this.pointsMap.get(city), '1');
        });

        eventOn(linkElement, 'mouseleave', (e: Event) => {
            e.preventDefault();
            this.pointsMap.forEach((pointElement: HTMLElement) => this.setOpacity(pointElement, '0'));
        });
    }

    /**
     * @method bindPointEventHandlers
     * @param {HTMLElement} pointElement
     * @private
     */
    private bindPointEventHandlers(pointElement: HTMLElement): void {

        eventOn(pointElement, 'mouseenter', (e: Event) => {
            e.preventDefault();
            this.setOpacity(pointElement, '1');
        });

        eventOn(pointElement, 'mouseleave', (e: Event) => {
            e.preventDefault();
            this.setOpacity(pointElement, '0');
        });

    }

}
